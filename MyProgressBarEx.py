from PySide2.QtCore import QObject, QTimer, QUrl, Slot, Signal
from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine


class Foo(QObject):

    def __init__(self):
       QObject.__init__(self)
       self.timer = QTimer()
       self.timer.timeout.connect(self.emitsignal)

    incPosition = Signal()
    def emitsignal(self):
       self.incPosition.emit()

    @Slot()
    def test_slot(self):
       self.timer.start(1000) 
           
              


if __name__ == "__main__":
    import os
    import sys

    app = QGuiApplication()
    foo = Foo()
    engine = QQmlApplicationEngine()
    engine.rootContext().setContextProperty("foo", foo)
    qml_file = "app.qml"
    current_dir = os.path.dirname(os.path.realpath(__file__))
    filename = os.path.join(current_dir, qml_file)
    engine.load(QUrl.fromLocalFile(filename))
    if not engine.rootObjects():
        sys.exit(-1)
    sys.exit(app.exec_())

