import QtQuick 2.1
import QtQuick.Controls 2.1

ApplicationWindow {
    id:root
    visible: true
    signal reIncPosition()
    Column{
        anchors.fill:parent
        ProgressBar{
            id: progress
            height: 20
            anchors.left:parent.left
            anchors.right:parent.right
            anchors.leftMargin: 5
            anchors.rightMargin: 5
            to: 10

        }
        Button {
            height: 20
            width: 70
            anchors.horizontalCenter:parent.horizontalCenter
            onClicked: {
                progress.value= 0
                foo.test_slot()
            }
            text: "Process"
        }
    
    Component.onCompleted: foo.incPosition.connect(reIncPosition)

    Connections {
        target: root
        onReIncPosition: { 
            progress.value += 1
            console.log(progress.value)
        }
    }

    }

}
